package ie.adapt.myo_bridge.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Jonathan Rosca
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MyoInputMessage {
    public enum PoseType {
        UNKNOWN,
        REST,
        FIST,
        SPREAD,
        WAVE_IN,
        WAVE_OUT,
        DOUBLE_TAP
    }

    private PoseType pose;

    public MyoInputMessage() {}

    public MyoInputMessage(PoseType pose) {
        this.pose = pose;
    }

    public PoseType getPoseType() {
        return pose;
    }

    @Override
    public String toString() {
        return pose.toString();
    }
}
