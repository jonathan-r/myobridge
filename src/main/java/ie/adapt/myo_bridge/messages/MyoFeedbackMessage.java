package ie.adapt.myo_bridge.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Jonathan Rosca
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MyoFeedbackMessage {
    public enum ActionType {
        VIBRATE,
        UNLOCK,
        LOCK
    }

    private ActionType action;

    public MyoFeedbackMessage() {}

    public MyoFeedbackMessage(ActionType action) {
        this.action = action;
    }

    public ActionType getActionType() {
        return action;
    }

    @Override
    public String toString() {
        return action.toString();
    }
}
