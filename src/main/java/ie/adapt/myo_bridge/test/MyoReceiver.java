package ie.adapt.myo_bridge.test;

import eu.semaine.components.Component;
import eu.semaine.jms.message.SEMAINEMessage;
import eu.semaine.jms.message.SEMAINEXMLMessage;
import eu.semaine.jms.receiver.Receiver;
import eu.semaine.jms.sender.Sender;
import ie.adapt.myo_bridge.messages.MyoFeedbackMessage;
import ie.adapt.myo_bridge.messages.MyoInputMessage;
import util.XmlConverter;

import javax.jms.JMSException;
import javax.xml.bind.JAXBException;

/**
 * Created by fred on 6/13/2016.
 */
public class MyoReceiver extends Component {
    private Sender sender;

    public MyoReceiver () throws JMSException {
        super("MyoReceiver");
        sender = new Sender("semaine.data.myo_feedback", "MYO", this.getName());
        senders.add(sender);

        receivers.add(new Receiver("semaine.data.myo_input"));
    }

    @Override
    public void react(SEMAINEMessage message) throws Exception {
        XmlConverter xmlCon = XmlConverter.newInstance();
        try {
            MyoInputMessage m = xmlCon.unmarshall(message.getText(), MyoInputMessage.class);

            if (m.getPoseType() == MyoInputMessage.PoseType.FIST) {
                MyoFeedbackMessage feedback = new MyoFeedbackMessage(MyoFeedbackMessage.ActionType.VIBRATE);
                String back = xmlCon.marshall(feedback, MyoFeedbackMessage.class);
                sender.sendTextMessage(back, meta.getTime());
            }
        }
        catch (JMSException | JAXBException | RuntimeException e) {
            log.debug(e.getMessage(), e);
        }
    }
}
