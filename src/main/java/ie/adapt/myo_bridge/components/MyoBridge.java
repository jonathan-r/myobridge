package ie.adapt.myo_bridge.components;

import com.thalmic.myo.*;
import com.thalmic.myo.enums.*;
import eu.semaine.components.Component;
import eu.semaine.jms.message.SEMAINEMessage;
import eu.semaine.jms.receiver.Receiver;
import eu.semaine.jms.sender.Sender;
import ie.adapt.myo_bridge.messages.MyoFeedbackMessage;
import ie.adapt.myo_bridge.messages.MyoInputMessage;
import util.XmlConverter;

import javax.jms.JMSException;
import javax.xml.bind.JAXBException;
import java.util.LinkedList;

/**
 @author Jonathan Rosca
 */

public class MyoBridge extends Component {
    private class Collector implements DeviceListener {
        //timestamps are in arbitrary units
        private long firstEventTimestamp; //timestamp coming from myo
        private long firstEventTime; //system time in milliseconds from epoch
        private boolean gotFirstEvent = false;

        public Collector() {
        }

        private void firstEventCheck(long myoTimestamp) {
            if (!gotFirstEvent) {
                firstEventTimestamp = myoTimestamp; //microseconds
                firstEventTime = MyoBridge.this.meta.getTime();
            }
        }

        private long eventTime(long myoTimestamp) {
            firstEventCheck(myoTimestamp);
            return firstEventTime + (myoTimestamp - firstEventTimestamp) / 1000;
        }

        public void onPair(Myo myo, long timestamp, FirmwareVersion firm) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onUnpair(Myo myo, long timestamp) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onConnect(Myo myo, long timestamp, FirmwareVersion firm) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onDisconnect(Myo myo, long timestamp) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onArmSync(Myo myo, long timestamp, Arm arm, XDirection xdir) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onArmUnsync(Myo myo, long timestamp) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onUnlock(Myo myo, long timestamp) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onLock(Myo myo, long timestamp) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onPose(Myo myo, long timestamp, Pose pose) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
                //MyoBridge.this.currentPose = pose.getType();
                //MyoBridge.this.currentPoseStr = pose.toString();
                System.out.println("pose: " + pose.toString());

                long milli = eventTime(timestamp);
                LinkedList<PoseEvent> queue = MyoBridge.this.eventQueue;

                synchronized (queue) {
                    queue.addLast(new PoseEvent(pose.getType(), milli));
                }
            //}
        }

        public void onOrientationData(Myo myo, long timestamp, Quaternion rot) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onAccelerometerData(Myo myo, long timestamp, Vector3 accel) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onGyroscopeData(Myo myo, long timestamp, Vector3 gyro) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onRssi(Myo myo, long timestamp, int strength) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }

        public void onEmgData(Myo myo, long timestamp, byte[] data) {
            //if (myo == MyoBridge.this.myo) { //our myo
                firstEventCheck(timestamp);
            //}
        }
    }

    private class PollingThread extends Thread {
        public PollingThread() {

        }

        public void run() {
            while (!MyoBridge.this.exitRequested()) {
                if (MyoBridge.this.myo == null) {
                    Myo myo = MyoBridge.this.hub.waitForMyo(500);

                    if (myo != null) {
                        myo.unlock(UnlockType.UNLOCK_HOLD);
                        myo.vibrate(VibrationType.VIBRATION_MEDIUM);
                        MyoBridge.this.myo = myo;

                        synchronized (MyoBridge.this.log) {
                            MyoBridge.this.log.info("Got Myo");
                        }
                    }
                }
                else {
                    //poll a few times in between lock access
                    for (int i = 0; i < 100; i++) {
                        MyoBridge.this.hub.run(50);
                    }
                }
            }
        }
    }

    private static class PoseEvent {
        public PoseType type;
        public long when;

        public MyoInputMessage.PoseType messagePoseType() {
            switch (type) {
                case REST: return MyoInputMessage.PoseType.REST;
                case FIST: return MyoInputMessage.PoseType.FIST;
                case FINGERS_SPREAD: return MyoInputMessage.PoseType.SPREAD;
                case WAVE_IN: return MyoInputMessage.PoseType.WAVE_IN;
                case WAVE_OUT: return MyoInputMessage.PoseType.WAVE_OUT;
                case DOUBLE_TAP: return MyoInputMessage.PoseType.DOUBLE_TAP;
                default: return MyoInputMessage.PoseType.UNKNOWN;
            }
        }

        PoseEvent(PoseType t, long milli) {
            type = t;
            when = milli;
        }
    }

    private Hub hub; //can connect to multiple myos
    private Myo myo; //target device
    private PollingThread poller;
    private Collector collect;

    private Sender sender;

    //private PoseType currentPose = PoseType.REST;
    //private String currentPoseStr = "";

    protected LinkedList<PoseEvent> eventQueue = new LinkedList<PoseEvent>();

    public MyoBridge() throws JMSException {
        super("MyoBridge", true, false);

        try {
            hub = new Hub("ie.adapt.jr.myo_bridge");
        }
        catch (Exception e) {
            throw new JMSException("Could not create Myo hub: " + e.toString());
        }

        poller = new PollingThread();
        collect = new Collector();
        hub.addListener(collect);

        poller.start();

        //sender = new Sender("ie.adapt.myo_input", "MYO", this.getName());
        sender = new Sender("semaine.data.myo_input", "MYO", this.getName());

        senders.add(sender);

        //receivers.add(new Receiver("ie.adapt.myo_feedback"));
        receivers.add(new Receiver("semaine.data.myo_feedback"));
    }

    @Override
    protected void act() throws Exception {
        //TODO: might need to dampen gesture jitter

        synchronized (eventQueue) {
            while (!eventQueue.isEmpty()) {
                PoseEvent ev = eventQueue.getFirst();
                eventQueue.removeFirst();

                XmlConverter xmlCon = XmlConverter.newInstance();
                try {
                    MyoInputMessage m = new MyoInputMessage(ev.messagePoseType());
                    String doc = xmlCon.marshall(m, MyoInputMessage.class);

                    sender.sendTextMessage(doc, ev.when);
                }
                catch (JMSException | JAXBException | RuntimeException e) {
                    log.debug(e.getMessage(), e);
                }
            }
        }

        if (exitRequested()) {
            if (myo != null) {
                myo.lock();
            }
        }
    }

    @Override
    protected void react(SEMAINEMessage message) throws Exception {
        System.out.println("got message" + message.toString());

        XmlConverter xmlCon = XmlConverter.newInstance();
        try {
            MyoFeedbackMessage action = xmlCon.unmarshall(message.getText(), MyoFeedbackMessage.class);

            if (myo != null) {
                switch (action.getActionType()) {
                    case VIBRATE:
                        myo.vibrate(VibrationType.VIBRATION_MEDIUM);
                        break;

                    case UNLOCK:
                        myo.unlock(UnlockType.UNLOCK_HOLD);
                        break;

                    case LOCK:
                        myo.lock();
                        break;

                    default:
                        log.error("bad myo action type");
                }
            }
        }
        catch (JMSException | JAXBException | RuntimeException e) {
            log.debug(e.getMessage(), e);
        }
    }
}
